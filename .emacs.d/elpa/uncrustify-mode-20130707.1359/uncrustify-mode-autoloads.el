;;; uncrustify-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "uncrustify-mode" "uncrustify-mode.el" (23705
;;;;;;  51776 237137 428000))
;;; Generated autoloads from uncrustify-mode.el

(autoload 'uncrustify-mode "uncrustify-mode" "\
Automatically `uncrustify' when saving.

\(fn &optional ARG)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; uncrustify-mode-autoloads.el ends here
