;;; -*- no-byte-compile: t -*-
(define-package "magit-annex" "1.7.1" "Control git-annex from Magit" '((cl-lib "0.3") (magit "2.90.0")) :commit "21cb2927d672cc6bf631d8373a361b1766ccf004" :keywords '("vc" "tools") :authors '(("Kyle Meyer" . "kyle@kyleam.com") ("Rémi Vanicat" . "vanicat@debian.org")) :maintainer '("Kyle Meyer" . "kyle@kyleam.com") :url "https://github.com/magit/magit-annex")
