;;; -*- no-byte-compile: t -*-
(define-package "magit-gitflow" "2.2.3" "gitflow extension for magit" '((magit "2.1.0") (magit-popup "2.2.0")) :commit "cc41b561ec6eea947fe9a176349fb4f771ed865b" :keywords '("vc" "tools") :authors '(("Jan Tatarik" . "Jan.Tatarik@gmail.com")) :maintainer '("Jan Tatarik" . "Jan.Tatarik@gmail.com") :url "https://github.com/jtatarik/magit-gitflow")
