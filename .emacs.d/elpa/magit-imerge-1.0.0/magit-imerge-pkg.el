;;; -*- no-byte-compile: t -*-
(define-package "magit-imerge" "1.0.0" "Magit extension for git-imerge" '((emacs "24.4") (magit "2.10.0")) :commit "5b45efa65317886640c339d1c71d2b9e00e98b77" :keywords '("vc" "tools") :authors '(("Kyle Meyer" . "kyle@kyleam.com")) :maintainer '("Kyle Meyer" . "kyle@kyleam.com") :url "https://github.com/magit/magit-imerge")
