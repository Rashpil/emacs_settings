
(defun backward-kill-line (arg)
  "Kill ARG lines backward."
  (interactive "p")
  (kill-line (- 1 arg)))

(provide 'my-scripts)

(defun reformat-region (&optional b e)
	"Reformat region on Google style"
    (interactive "r")
    (when (not (buffer-file-name))
      (error "A buffer must be associated with a file in order to use REFORMAT-REGION."))
    (when (not (executable-find "clang-format"))
      (error "clang-format not found."))
    (shell-command-on-region b e
                                                "clang-format -style=Google"
                                                (current-buffer) t)
    (indent-region b e)
    )

(defun addcomment1(start end )
 (interactive)
 (setq issue (read-string "Enter issue number:"))
 (save-excursion
  (goto-char end) (insert "\n/* ~Nesin Rostislav */")
  (goto-char start) (insert "/* Nesin Rostislav: #"issue", "(shell-command-to-string "echo -n $(date +%d.%m.%Y)")" */\n")
 )
)

(defun comment1(start end)
 (interactive "r")
 (addcomment1 start end)
)

(defun comment2()
(interactive)
(setq issue (read-string "Enter issue number:"))
 (end-of-line) (insert " // Nesin Rostislav: #"issue", "(shell-command-to-string "echo -n $(date +%d.%m.%Y)"))
)
